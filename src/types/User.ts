export default interface Product {
  id?: number;
  login: string;
  name: string;
  password: string;
  createDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
