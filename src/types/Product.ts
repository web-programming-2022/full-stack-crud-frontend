export default interface Product {
  id?: number;
  name: string;
  price: number;
  createDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
