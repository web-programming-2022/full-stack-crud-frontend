import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/users";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });

  async function getUser() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUser();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;

      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก User ได้");
    }
    loadingStore.isLoading = false;
  }

  function editUser(product: User) {
    loadingStore.isLoading = true;
    editedUser.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
    loadingStore.isLoading = false;
  }

  return {
    users,
    getUser,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
  };
});
